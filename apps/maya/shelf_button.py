from pymel.core import *
from multi_script_editor.main_dialog import MultiScriptEditorDialog


def open_in_doc_control(title='Multi Script Editor', position='right'):     # or left
    qmaya = ui.PyUI('MayaWindow').asQtObject()
    obj_name = 'multi_script_editor'
    doc_name = 'doc_script_editor'
    # cleanup
    if window(obj_name, q=1, ex=1):
        deleteUI(obj_name)
    if dockControl(doc_name, q=1, ex=1):
        deleteUI(doc_name)
    # create
    dialog = MultiScriptEditorDialog(parent=qmaya, session_name='maya')
    dialog.setObjectName(obj_name)
    dockControl(doc_name, area=position,
                content=obj_name,
                width=700,
                label=title,
                allowedArea=['right', 'left'])


def open_as_window():
    qmaya = ui.PyUI('MayaWindow').asQtObject()
    dialog = MultiScriptEditorDialog(parent=qmaya, session_name='maya')
    dialog.show()
