from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from multi_script_editor.main_dialog import MultiScriptEditorDialog
import nuke
qApp = QApplication.instance()


def getMainWindow():
    for widget in qApp.topLevelWidgets():
        if widget.metaObject().className() == 'Foundry::UI::DockMainWindow':
            return widget

qNuke = getMainWindow()


class ScriptEditorReplacer(QObject):
    def eventFilter(self, obj, ev):
        if not ev.type() in [QEvent.Paint, QEvent.LayoutRequest, QEvent.UpdateRequest, QEvent.WindowDeactivate,
                             QEvent.HoverMove, QEvent.Leave, QEvent.HoverLeave, QEvent.Enter, QEvent.KeyRelease]:
            nuke.tprint(ev.type())
        if ev.type() == QEvent.ChildPolished:
            print(ev.child().objectName())
            if ev.child().objectName().startswith('uk.co.thefoundry.scripteditor.'):
                self.addPanel(ev.child())
        return False

    @classmethod
    def addPanel(cls, widget):
        if not widget.findChild(QWidget, 'multi_script_editor'):
            map(lambda x: x.hide(), widget.findChildren(QPushButton))
            map(lambda x: x.hide(), widget.findChildren(QSplitter))
            ly = widget.layout()
            mse = MultiScriptEditorDialog(parent=qNuke, session_name='nuke')
            mse.setObjectName('multi_script_editor')
            ly.addWidget(mse)

# f = ScriptEditorReplacer()
# print('INSTALL FILTER')
# qNuke.installEventFilter(f)

mse = MultiScriptEditorDialog(parent=qNuke, session_name='nuke')
mse.show()
