import os

style_path = os.path.join(os.path.dirname(__file__), 'style.css')
if os.path.exists(style_path):
    qss = open(style_path).read()
else:
    qss = ''
