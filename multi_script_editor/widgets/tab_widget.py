from __future__ import absolute_import
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from. editor import CodeEditor


class CodeTabWidget(QTabWidget):
    executeSignal = Signal(str)

    def __init__(self, *args):
        super(CodeTabWidget, self).__init__(*args)
        self.setTabsClosable(True)
        self.setMovable(True)
        self.tabCloseRequested.connect(self.close_tab)
        self.tabBar().setContextMenuPolicy(Qt.CustomContextMenu)
        self.tabBar().customContextMenuRequested.connect(self.open_menu)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.open_menu)

        new_tab_btn = QPushButton('+', self, clicked=self.add_new_code_tab)
        new_tab_btn.setToolTip("Add Tab")
        new_tab_btn.setMaximumWidth(30)
        self.setCornerWidget(new_tab_btn, Qt.TopRightCorner)

    def add_new_code_tab(self, tab_text=None, source_code=None):
        editor = CodeEditor(self)
        editor.executeSignal.connect(self.executeSignal.emit)
        if source_code:
            editor.setText(source_code)
        self.addTab(editor, tab_text or 'New Tab')
        self.setCurrentIndex(self.count() - 1)

    def get_current_selection(self):
        w = self.widget(self.currentIndex())    # type: CodeEditor
        return w.get_selected()

    def get_current_text(self):
        w = self.widget(self.currentIndex())  # type: CodeEditor
        return w.text()

    def get_tab_text(self, i):
        w = self.widget(i)  # type: CodeEditor
        return w.text()

    def get_session(self):
        data = []
        for i in range(self.count()):
            text = self.widget(i).text()
            title = self.tabText(i)
            data.append(dict(
                text=text, title=title
                # todo: save cursor position and scroll
            ))
        return data

    def set_session(self, data):
        self.clear()
        if not data:
            self.add_new_code_tab()
        else:
            for tab in data:
                self.add_new_code_tab(tab.get('title', 'New Tab'), tab.get('text'))

    def close_tab(self, i=None):
        i = i if i is not None else self.currentIndex()
        print(i)
        if self.count() > 1:
            if self.get_tab_text(i):
                if self._yes_no_question('Close this tab without saving?\n'+self.tabText(i)):
                    self.removeTab(i)
            else:
                self.removeTab(i)

    def open_menu(self):
        menu = QMenu(self)
        menu.addAction(QAction('Add Tab', self, triggered=self.add_new_code_tab))
        menu.addAction(QAction('Rename Current Tab', self, triggered=self.rename_tab))
        menu.addAction(QAction('Close Current Tab', self, triggered=self.close_tab))
        menu.exec_(QCursor.pos())

    def rename_tab(self):
        index = self.currentIndex()
        text = self.tabText(index)
        result = QInputDialog.getText(self, 'New name', 'Enter New Name', text=text)
        if result[1]:
            self.setTabText(index, result[0])

    def _yes_no_question(self, question):
        msg_box = QMessageBox(self)
        msg_box.setText(question)
        yes_button = msg_box.addButton("Yes", QMessageBox.YesRole)
        no_button = msg_box.addButton("No", QMessageBox.NoRole)
        msg_box.exec_()
        return msg_box.clickedButton() == yes_button

    def set_focus_to_current(self):
        self.widget(self.currentIndex()).setFocus()