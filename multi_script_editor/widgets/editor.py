from __future__ import absolute_import
from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from ..syntax.highlighter import PythonHighlighterClass
from .. import settings
from ..managers import context
import logging, re

logger = logging.getLogger(__name__)


_escapeButtons = [Qt.Key_Return, Qt.Key_Enter, Qt.Key_Left, Qt.Key_Right, Qt.Key_Home, Qt.Key_End, Qt.Key_PageUp,
                  Qt.Key_PageDown, Qt.Key_Delete, Qt.Key_Insert, Qt.Key_Escape]
font_name = settings.get('font_name', 'Courier')
indentLen = 4
minimumFontSize = 10
default_font_size = settings.get('default_font_size', 14)


class CodeEditor(QTextEdit):
    executeSignal = Signal(str)

    def __init__(self, *args, **kwargs):
        super(CodeEditor, self).__init__(*args)
        self.setWordWrapMode(QTextOption.NoWrap)
        font = QFont(font_name)
        font.setStyleHint(QFont.Monospace)
        font.setFixedPitch(True)
        font.setPointSize(default_font_size)
        self.setFont(font)
        self.document().setDefaultFont(QFont(font_name, default_font_size, QFont.Monospace))
        metrics = QFontMetrics(self.document().defaultFont())
        self.setTabStopWidth(4 * metrics.width(' '))

        self.add_shortcuts()
        self._apply_syntax_highlighter()
        self.callback = kwargs.get('save_callback')

        self.completer = None

    def _apply_syntax_highlighter(self):
        from ..syntax.design import get_colors
        colors = get_colors()
        self.sh = PythonHighlighterClass(self, colors)

    def _apply_style_sheet(self):
        style = 'background-color: #2B2B2B'
        self.setStyleSheet(style)

    def add_shortcuts(self):

        self.exec_act = QAction('Execute Selected', self)
        self.exec_act.triggered.connect(self.execute_selected)
        self.exec_act.setShortcut(QKeySequence('Ctrl+Return'))
        self.exec_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.exec_act)

        self.exec_all_act = QAction('Execute All', self)
        self.exec_all_act.triggered.connect(self.execute_all)
        self.exec_all_act.setShortcut(QKeySequence('Ctrl+Shift+Return'))
        self.exec_all_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.exec_all_act)

        self.comment_act = QAction('Comment', self)
        self.comment_act.triggered.connect(self.comment_selected)
        self.comment_act.setShortcut(QKeySequence('Ctrl+/'))
        self.comment_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.comment_act)

        self.duplicate_act = QAction('Duplicate', self)
        self.duplicate_act.triggered.connect(self.duplicate)
        self.duplicate_act.setShortcut(QKeySequence('Ctrl+D'))
        self.duplicate_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.duplicate_act)

        self.sb_act = QAction('Select Block', self)
        self.sb_act.triggered.connect(self._select_blocks)
        self.sb_act.setShortcut(QKeySequence('Ctrl+B'))
        self.sb_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.sb_act)

        self.find_act = QAction('Find', self)
        self.find_act.triggered.connect(self.find_dialog)
        self.find_act.setShortcut(QKeySequence('Ctrl+F'))
        self.find_act.setShortcutContext(Qt.WidgetShortcut)
        self.addAction(self.find_act)

    # tools
    def get_selected(self, full_lines=False):
        cursor = self.textCursor()
        if full_lines:
            self._select_blocks()
        sel = self.toPlainText()[cursor.selectionStart():cursor.selectionEnd()]
        return sel

    def get_current_line(self):
        self.document().documentLayout().blockSignals(True)
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.StartOfLine)
        cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
        text = cursor.selectedText()
        self.document().documentLayout().blockSignals(False)
        return text

    def add_indent(self, count):
        if count > 0:
            self.insertPlainText(' '*indentLen)

    def execute_selected(self):
        selected = self.get_selected()
        if selected.strip():
            if self.callback:
                self.callback(self.text())
            else:
                self.executeSignal.emit(selected)

    def execute_all(self):
        text = self.text()
        if text.strip():
            if self.callback:
                self.callback(self.text())
            else:
                self.executeSignal.emit(text)

    def duplicate(self):
        logger.debug('Duplicate')
        self.document().documentLayout().blockSignals(True)
        cursor = self.textCursor()
        if cursor.hasSelection():   # duplicate selected
            sel = cursor.selectedText()
            end = cursor.selectionEnd()
            cursor.setPosition(end)
            cursor.insertText(sel)
            cursor.setPosition(end,QTextCursor.KeepAnchor)
            self.setTextCursor(cursor)
        else:   # duplicate line
            cursor.movePosition(QTextCursor.StartOfLine)
            cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
            line = cursor.selectedText()
            cursor.clearSelection()
            cursor.insertText('\n'+line)
            self.setTextCursor(cursor)
        self.document().documentLayout().blockSignals(False)
        self.repaint()

    def find_dialog(self):
        print('FIND')

    def hide_completer(self):
        pass

    def keyPressEvent(self, event):
        post_insert = ''
        if event.key() == Qt.Key_Tab:
            if self._move_selected(True):
                return
            else:
                self.insertPlainText(' '*indentLen)
            return
        elif event.key() == Qt.Key_Backtab:
            self._move_selected(False)
            return
        if event.key() in _escapeButtons:
            self.hide_completer()
        if event.key() in [Qt.Key_Enter, Qt.Key_Return]:
            text = self.get_current_line()
            steps = 0
            if text.split('#')[0].endswith(':'):
                steps += 1
            current_indent = re.search(r'^\s+', text)
            if current_indent:
                steps += len(current_indent.group(0))//indentLen
            post_insert = (' '*indentLen)*steps

            if self.completer and self.completer.isVisible():
                self.hide_completer()
                # todo: apply completer first line
        elif event.modifiers() == Qt.NoModifier and event.key() == Qt.Key_Backspace:
            if self._delete_one_indent():
                return
        super(CodeEditor, self).keyPressEvent(event)
        if post_insert:
            self.insertPlainText(post_insert)

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            if self.completer:
                self.completer.updateCompleteList()
            if event.delta() > 0:
                self.change_font_size(True)
            else:
                self.change_font_size(False)
        else:
            super(CodeEditor, self).wheelEvent(event)

    def text(self):
        return self.toPlainText()

    def change_font_size(self, up):
        if context == 'hou':    # todo: move to manager
            if up:
                self.fs = min(30, self.fs+1)
            else:
                self.fs = max(8, self.fs - 1)
            self.setTextEditFontSize(self.fs)
        else:
            f = self.font()
            size = f.pointSize()
            if up:
                size = min(172, size+1)
            else:
                size = max(8, size - 1)
            f.setPointSize(size)
            f.setFamily(font_name)
            self.setFont(f)

    def insertFromMimeData(self, source):
        text = source.text()
        self.insertPlainText(text)

    def comment_selected(self):
        cursor = self.textCursor()
        self.document().documentLayout().blockSignals(True)
        self._select_blocks()
        pos = cursor.position()
        start = cursor.selectionStart()
        end = cursor.selectionEnd()
        cursor.setPosition(start)
        cursor.movePosition(QTextCursor.StartOfLine)
        cursor.setPosition(end, QTextCursor.KeepAnchor)
        cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
        text = cursor.selection().toPlainText()
        self.document().documentLayout().blockSignals(False)
        # cursor.removeSelectedText()
        text, offset = self._add_remove_comments(text)
        cursor.insertText(text)
        cursor.setPosition(min(pos+offset, len(self.toPlainText())))
        self.setTextCursor(cursor)
        self.update()

    def _remove_tabs(self, text):
        lines = text.split('\n')
        new = []
        pat = re.compile("^ .*")
        for line in lines:
            line = line.replace('\t', ' '*indentLen)
            for _ in range(4):
                if pat.match(line):
                    line = line[1:]
            new.append(line)
        return '\n'.join(new)

    def _add_tabs(self, text):
        lines = [(' '*indentLen)+x for x in text.split('\n')]
        return '\n'.join(lines)

    def _move_selected(self, inc):
        if self.textCursor().hasSelection() or not inc:
            self.document().documentLayout().blockSignals(True)
            self._select_blocks()
            cursor = self.textCursor()
            start, end = cursor.selectionStart(), cursor.selectionEnd()
            text = cursor.selection().toPlainText()
            cursor.removeSelectedText()
            if inc:
                newText = self._add_tabs(text)
            else:
                newText = self._remove_tabs(text)
            cursor.beginEditBlock()
            cursor.insertText(newText)
            cursor.endEditBlock()
            newEnd = cursor.position()
            cursor.setPosition(start)
            cursor.setPosition(newEnd, QTextCursor.KeepAnchor)
            if not text.strip():
                cursor.clearSelection()
            self.document().documentLayout().blockSignals(False)
            self.setTextCursor(cursor)
            self.update()
            return True
        return False

    def _select_blocks(self):
        self.document().documentLayout().blockSignals(True)
        cursor = self.textCursor()
        start, end = cursor.selectionStart(), cursor.selectionEnd()
        cursor.setPosition(start)
        cursor.movePosition(QTextCursor.StartOfLine)
        cursor.setPosition(end, QTextCursor.KeepAnchor)
        cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
        self.setTextCursor(cursor)
        self.document().documentLayout().blockSignals(False)

    def _add_remove_comments(self, text):
        result = text
        ofs = 0
        if text.strip():
            lines = text.split('\n')
            ind = 0
            while not lines[ind].strip():
                ind += 1
            if lines[ind].strip()[0] == '#': # remove comment
                result = '\n'.join([x.replace('#', '', 1) for x in lines])
                ofs = -1
            else:   # add comment
                result = '\n'.join(['#'+x for x in lines ])
                ofs = 1
        return result, ofs

    def _delete_one_indent(self):
        cursor = self.textCursor()
        cursor.setPosition(cursor.position()-indentLen, QTextCursor.KeepAnchor)
        if cursor.selectedText().strip():
            return False
        cursor.removeSelectedText()
        self.setTextCursor(cursor)
        return True
