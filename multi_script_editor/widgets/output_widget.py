from PySide2.QtWidgets import *
from PySide2.QtGui import *

font_name = 'Courier'


class OutputWindow(QTextBrowser):
    def __init__(self, *args):
        super(OutputWindow, self).__init__(*args)
        self.setWordWrapMode(QTextOption.NoWrap)
        font = QFont("Courier")
        font.setStyleHint(QFont.Monospace)
        font.setFixedPitch(True)
        self.setFont(font)
        self.document().setDefaultFont(QFont(font_name, 14, QFont.Monospace))
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.open_menu)

    def open_menu(self):
        menu = self.createStandardContextMenu()
        menu.addAction(QAction('Clear', self, triggered=self.clear))
        menu.exec_(QCursor.pos())
