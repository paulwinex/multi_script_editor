# coding=utf-8
"""
Настройки
"""
import os, json


SETTINGS_DIR = os.path.expanduser('~/.mse')
_settings_file_name = 'settings.json'
_settings_file_path = os.path.join(SETTINGS_DIR, _settings_file_name)


def read():
    if os.path.exists(_settings_file_path):
        return json.load(open(_settings_file_path))
    else:
        return {}


def save(data):
    json.dump(data, open(_settings_file_path, 'w'), indent=2)


def get(key, default=None):
    return read().get(key, default)


def set(key, value):
    data = read()
    data[key] = value
    save(data)


def update(data):
    s = read()
    s.update(data)
    save(s)
