# coding=utf-8
"""
Основной виджет
"""
from __future__ import absolute_import
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from .widgets.output_widget import OutputWindow
from .widgets.tab_widget import CodeTabWidget
from .utils.pyexecutor import execute_code
from .managers import context
from . import session


class MultiScriptEditorDialog(QMainWindow):
    def __init__(self, parent=None, session_name=None, *args):
        super(MultiScriptEditorDialog, self).__init__(parent, *args)
        add_btn = False
        self.setWindowTitle('Multi Script Editor')
        self._session_name = session_name

        self.central_widget = QWidget(self)
        self.main_ly = QVBoxLayout(self.central_widget)
        self.main_ly.setContentsMargins(5, 5, 5, 5)
        if add_btn:
            self.buttons_ly = QHBoxLayout(self)
            self.main_ly.addLayout(self.buttons_ly)
            self.execute_all_btn = QPushButton('⯈⯈')
            self.buttons_ly.addWidget(self.execute_all_btn)
            # self.execute_all_btn.setFlat(True)
            self.execute_sel_btn = QPushButton('⯈')
            self.buttons_ly.addWidget(self.execute_sel_btn)

            self.clear_btn = QPushButton('⎚')
            self.buttons_ly.addWidget(self.clear_btn)

            # self.execute_sel_btn.setFlat(True)
            self.buttons_ly.addItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self.splitter = QSplitter(self.central_widget)
        self.splitter.setOrientation(Qt.Vertical)
        self.output_tb = OutputWindow(self.splitter)
        self.ly_top = QVBoxLayout(self.output_tb)
        self.ly_top.setContentsMargins(0, 0, 0, 0)
        self.tabs_tw = CodeTabWidget(self.splitter)
        self.tabs_tw.executeSignal.connect(self.on_execute_command)
        self.ly_bottom = QVBoxLayout(self.tabs_tw)
        self.ly_bottom.setContentsMargins(0, 0, 0, 0)
        self.main_ly.addWidget(self.splitter)
        self.setCentralWidget(self.central_widget)

        self.menubar = QMenuBar(self)
        # self.menubar.setGeometry(QRect(0, 0, 800, 21))
        self.menuFIle = QMenu('File', self.menubar)
        self.menuCode = QMenu('Code', self.menubar)
        self.menuHelp = QMenu('Help', self.menubar)
        self.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(self)
        self.setStatusBar(self.statusbar)
        self.actionExecute_All = QAction('Execute All', self)
        self.actionExecute_Selected = QAction('Execute Selected', self)
        self.actionSave_Session = QAction('Save Session...', self, triggered=self.save_session_to_file)
        self.actionLoad_Session = QAction('Load Session...', self, triggered=self.load_session_from_file)
        self.actionExit = QAction('Exit', self, triggered=self.close)
        self.actionExecute_Selected_2 = QAction('Execute Selected', self)
        self.actionExecute_All_2 = QAction('Execute All', self)
        self.actionAbout = QAction('About', self)
        self.actionManual = QAction('Manual', self)
        self.menuFIle.addAction(self.actionSave_Session)
        self.menuFIle.addAction(self.actionLoad_Session)
        self.menuFIle.addSeparator()
        self.menuFIle.addAction(self.actionExit)
        self.menuCode.addAction(self.actionExecute_Selected_2)
        self.menuCode.addAction(self.actionExecute_All_2)
        self.menuHelp.addAction(self.actionManual)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuFIle.menuAction())
        self.menubar.addAction(self.menuCode.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.right_bar = QMenuBar()
        self.right_bar.addAction(QAction('⯈', self, triggered=self.on_execute_selected_command))

        self.right_bar.addAction(QAction('⯈⯈', self, triggered=self.on_execute_all_command))
        self.right_bar.addAction(QAction('⨯', self, triggered=self.output_tb.clear))
        # self.right_bar.addMenu(self.right_menu)
        self.menubar.setCornerWidget(self.right_bar, Qt.TopRightCorner)

        self.resize(800, 600)
        if not context:
            self._apply_style()
        # init
        self.load_session()
        self.tabs_tw.set_focus_to_current()

    def _apply_style(self):
        from .style import qss
        self.setStyleSheet(qss)

    def on_execute_command(self, source_code):
        if source_code.strip():
            self.save_session()
            self.output_tb.append('\n'.join(['>>> '+x for x in source_code.split('\n')]))
            execute_code(source_code, self.output_tb.append, dict(self=self))

    def on_execute_all_command(self):
        code = self.tabs_tw.get_current_text()
        self.on_execute_command(code)

    def on_execute_selected_command(self):
        code = self.tabs_tw.get_current_selection()
        self.on_execute_command(code)

    def closeEvent(self, event):
        self.save_session()
        super(MultiScriptEditorDialog, self).closeEvent(event)

    def save_session(self):
        code_session = self.tabs_tw.get_session()
        data = dict(
            pos=[self.pos().x(), self.pos().y()],
            size=[self.size().width(), self.size().height()],
            tabs=code_session,
            current_tab=self.tabs_tw.currentIndex()
        )
        session.save(data, self._session_name)

    def load_session(self, with_geo=True):
        data = session.load(self._session_name)
        self.tabs_tw.set_session(data.get('tabs', []))
        try:
            self.tabs_tw.setCurrentIndex(data.get('current_tab', 0))
        except:
            pass
        if with_geo:
            x,  y = data.get('pos', [None, None])
            if x and y:
                self.move(x, y)
            w, h = data.get('size', [None, None])
            if w and h:
                self.resize(w, h)

    def load_session_from_file(self):
        pass

    def save_session_to_file(self):
        pass
