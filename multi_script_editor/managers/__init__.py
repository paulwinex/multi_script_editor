

def define_context():
    ctx = ''
    try:
        import hou
        ctx = 'hou'
    except ImportError:
        pass
    try:
        import maya.cmds
        ctx = 'maya'
    except ImportError:
        pass
    try:
        import MaxPlus
        ctx = 'max'
    except ImportError:
        pass
    # todo: nuke, mari, hiero, blender
    return context

context = ''
