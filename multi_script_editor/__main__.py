# coding=utf-8
"""
Запуск как стендалон
"""
from PySide2.QtWidgets import QApplication
import logging
logging.basicConfig(level=logging.DEBUG)
from multi_script_editor import main_dialog

app = QApplication([])
w = main_dialog.MultiScriptEditorDialog()
w.show()
app.exec_()
