# coding=utf-8
"""
Сохранение и загрузка сессий
"""
from __future__ import absolute_import
import os
import json
from .settings import SETTINGS_DIR

SESSION_DIR = os.path.join(SETTINGS_DIR, 'sessions')


def save(data, name=None):
    path = _get_path(name)
    _mkdir()
    json.dump(data, open(path, 'w'))


def load(name=None):
    path = _get_path(name)
    if os.path.exists(path):
        return json.load(open(path))
    else:
        return {}


def _get_path(name):
    return os.path.join(SESSION_DIR, (_normalize_name(name) if name else 'default') + '.json')


def _mkdir():
    if not os.path.exists(SESSION_DIR):
        os.makedirs(SESSION_DIR)


def _normalize_name(name):
    return name.replace(' ', '_').lower()
