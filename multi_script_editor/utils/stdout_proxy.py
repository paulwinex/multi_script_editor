from PySide2.QtCore import QCoreApplication
import sys


class StdoutProxy(object):
    def __init__(self, callback):
        self.clb = callback
        self._tmp = None
        self.out = None

    def __enter__(self):
        self._tmp = sys.stdout
        self.out = StdoutWrapper(self.clb)
        sys.stdout = self.out

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._tmp
        self._tmp = None


class StdoutWrapper(object):
    def __init__(self, write_func):
        self.write_func = write_func
        self.skip = False

    def write(self, text):
        if not self.skip:
            stripped_text = text
            self.write_func(stripped_text)
            QCoreApplication.processEvents()
        self.skip = not self.skip

    def flush(self):
        pass

