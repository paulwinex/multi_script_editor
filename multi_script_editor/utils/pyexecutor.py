# coding=utf-8
"""
Исполнение кода
"""
import sys
from .stdout_proxy import StdoutProxy
import traceback
PY = sys.version_info.major


def execute_code(code, output, extra_ns=None):
    ns = __import__('__main__').__dict__
    if extra_ns:
        ns.update(extra_ns)
    try:
        with StdoutProxy(output):
            try:
                res = eval(code, ns, ns)
                if res is not None:
                    print(res)
            except SyntaxError:
                exec(code, ns)
    except SystemExit:
        pass
    except Exception as e:
        traceback_lines = traceback.format_exc().split('\n')
        for i in (3, 2, 1, -1):
            traceback_lines.pop(i)
        output('\n'.join(traceback_lines))


